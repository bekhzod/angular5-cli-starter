import {CurrentUserEntity} from '../models/auth-model';
import {Credentials} from '../../../common/config/types';
import {TokenEntity} from '../../../common/models/base.model';

export enum AuthActionsEnum {
  GetToken = '[Auth] GetToken',
  GetTokenFailed = '[Auth] GetTokenFailed',
  GetTokenSuccess = '[Auth] GetTokenSuccess',
  CheckToken = '[Auth] CheckToken',
  CheckTokenSuccess = '[Auth] CheckTokenSuccess',
  CheckTokenFailed = '[Auth] CheckTokenFailed',
  GetCurrentUser = '[Auth] GetCurrentUser',
}

export class GetToken {
  public readonly type= AuthActionsEnum.GetToken;

  constructor(public payload: { credentials: Credentials }) {
  }
}

export class GetTokenFailed {
  public readonly type= AuthActionsEnum.GetTokenFailed;

  constructor(public payload: { error: String }) {
  }
}

export class GetTokenSuccess {
  public readonly type= AuthActionsEnum.GetTokenSuccess;

  constructor(public payload: { token: TokenEntity }) {
  }
}

export class CheckToken {
  readonly type= AuthActionsEnum.CheckToken;

  constructor(public payload: { token: String }) {
  }
}

export class CheckTokenSuccess {
  readonly type= AuthActionsEnum.CheckTokenSuccess;

  constructor(public payload: { token: string }) {
  }
}

export class CheckTokenFailed {
  readonly type= AuthActionsEnum.CheckTokenFailed;

  constructor(public payload: { error: string }) {
  }
}

export class GetCurrentUser {
  readonly type= AuthActionsEnum.GetCurrentUser;

  constructor(public payload: { currentUser: CurrentUserEntity }) {
  }
}

export type AuthActionsList =
  | GetCurrentUser
  | CheckTokenFailed
  | CheckTokenSuccess
  | CheckToken
  | GetToken
  | GetTokenSuccess
  | GetTokenFailed
