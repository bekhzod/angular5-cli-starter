import {createEntityAdapter, EntityAdapter} from '@ngrx/entity';
import {AuthEntity} from '../models/auth-model';

export const adapter: EntityAdapter<AuthEntity> = createEntityAdapter<AuthEntity>({
  sortComparer: false
});
