import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {AuthGuard} from '../../../common/service/guards/auth-service';
import {AuthActionsEnum, GetToken, GetTokenFailed, GetTokenSuccess} from '../actions/auth-actions';
import {catchError, exhaustMap, map} from 'rxjs/internal/operators';
import {of} from 'rxjs';
import {TokenEntity} from '../../../common/models/base.model';

@Injectable(
  {providedIn: 'root'}
)
export class AuthEffects {
  constructor(private actions$: Actions, private authService: AuthGuard) {

  }

  @Effect()
  generateToken$ = this.actions$.pipe(
    ofType<GetToken>(AuthActionsEnum.GetToken),
    map(action => action.payload),
    exhaustMap(({credentials}) => this.authService.generate(credentials.login, credentials.password)
      .pipe(
        map((object: TokenEntity) => new GetTokenSuccess({token: object})),
        catchError(error => of(new GetTokenFailed({error})))
      ))
  );
}
