import {BaseEntity} from '../../../common/models/base.model';

export interface AuthEntity {
  user: CurrentUserEntity
}

export interface CurrentUserEntity extends BaseEntity {
  username?: String
  permissions?: String[]
}
