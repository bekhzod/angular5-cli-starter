import * as fromAdapter from '../adapter/index';
import {EntityState} from '@ngrx/entity';
import {AuthEntity} from '../models/auth-model';
import {AuthActionsEnum, AuthActionsList} from '../actions/auth-actions';
import {TokenEntity} from '../../../common/models/base.model';

export interface State extends EntityState<AuthEntity> {
  auth: AuthEntity,
  token: TokenEntity,
  error: String
}

const auth: AuthEntity = {user: null};

export const initialState: State = fromAdapter.adapter.getInitialState({
  error: null,
  auth,
  token: null
});

export function reducer(state = initialState, action: AuthActionsList) {
  console.log(action.type);
  switch (action.type) {
    case AuthActionsEnum.GetToken: {

      return state;
    }
    case AuthActionsEnum.CheckToken: {
      return state;
    }
    case AuthActionsEnum.CheckTokenSuccess: {
      return state;
    }
    case AuthActionsEnum.CheckTokenFailed: {
      return state;
    }
    case AuthActionsEnum.GetCurrentUser: {
      return state;
    }
    case AuthActionsEnum.GetTokenSuccess: {
      console.log('state', state);
      return {...state, token: action.payload.token};
    }
    default: {
      return state;
    }
  }
}

export const getAuth = (state: State) => state.auth;

export const getCurrentUser = (auth: AuthEntity) => auth.user;

export const getToken = (state: State) => state.token;
