import * as fromAuthReducer from './auth-reducers';
import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import {getAuth, getCurrentUser, getToken} from './auth-reducers';


export interface AuthState {
  auth: fromAuthReducer.State
}

export interface State {
  authState: AuthState
}

export const reducers: ActionReducerMap<AuthState> = {
  auth: fromAuthReducer.reducer
};

export const selectAuthState = createFeatureSelector<AuthState>('Auth');

export const selectAuth = createSelector(selectAuthState, (state: AuthState) => state.auth);

export const selectEntity = createSelector(selectAuth, getAuth);

export const selectCurrentUser = createSelector(selectEntity, getCurrentUser);

export const getTokenState = createSelector(selectAuth, getToken);
