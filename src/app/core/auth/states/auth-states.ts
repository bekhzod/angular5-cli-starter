import {EntityState} from '@ngrx/entity';
import {AuthEntity} from '../models/auth-model';

export interface AuthUserStates extends EntityState<AuthEntity> {

}
