import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from "../dashboard/dashboard.component";
import {HomeComponent} from "../components/home/home.component";
import {LoginComponent} from "../login/login.component";
import {AuthGuard} from "../common/service/guards/auth-service";
import {AuthGuardChild} from "../common/service/guards/auth-guard-child";
import {TempComponent} from "../components/temp/temp.component";

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuardChild],
    children: [
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'temp',
        component: TempComponent
      },
      {
        path: 'users',
        loadChildren: 'src/app/modules/users/users.module#UsersModule'
      },
      {
        path: 'films',
        loadChildren: 'src/app/modules/films/films.module#FilmsModule'
      }
    ]
  }
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes, {useHash: true, onSameUrlNavigation: 'reload'})
  ],
  exports: [RouterModule],
})
export class AppRouterModule {
}
