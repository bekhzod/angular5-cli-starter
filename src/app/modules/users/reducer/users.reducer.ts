import * as fromAdapter from '../adapter/index';
import {EntityState} from '@ngrx/entity';
import {UserDetailsEntity} from '../models/users.model';
import {UsersActionsEnum, UsersActionsList} from '../actions/users-actions';
import {objectToArray} from '../../../common/config/utils';

export interface State extends EntityState<UserDetailsEntity> {
  error: String | null;
  users: UserDetailsEntity[];
}

export const initialState: State = fromAdapter.adapter.getInitialState({
  error: null,
  users: []
});

export function reducer(state = initialState, action: UsersActionsList): State {
  console.log('type', action.type);
  switch (action.type) {
    case UsersActionsEnum.GetUserListSuccess: {
      return {...state, users: action.payload.userList};
    }
    case UsersActionsEnum.GetUserListFailed: {
      return {...state, error: action.payload.error};
    }
    case UsersActionsEnum.SaveUserSuccess: {
      const users = state.users;
      users.push(action.payload.user);
      return {...state, users};
    }
    case UsersActionsEnum.SaveUserFailed: {
      return {...state, error: action.payload.error};
    }
    default: {
      return state;
    }
  }
}

export const getUserList = (state: State) => objectToArray(state.users);

export const getUserError = (state: State) => state.error;
