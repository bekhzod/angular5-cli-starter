import * as fromUsers from './users.reducer'
import * as fromAdapter from '../adapter/index';
import { ActionReducerMap, createFeatureSelector, createSelector } from "@ngrx/store";

export interface UsersState {
  userState: fromUsers.State
}

export interface State {
  user: UsersState
}

export const reducers: ActionReducerMap<UsersState> = {
  userState: fromUsers.reducer
};

export const selectUsersState = createFeatureSelector<UsersState>('users');

export const getUsersState = createSelector(selectUsersState, (state: UsersState) => state.userState);

export const getUsersList = createSelector(getUsersState, fromUsers.getUserList);

export const getUsersError = createSelector(getUsersState, fromUsers.getUserError);

export const getUsersIds = createSelector(getUsersState, fromAdapter.selectUsersIds);

export const getAllUsers = createSelector(getUsersState, fromAdapter.selectAllUsers);

export const getUsersCount = createSelector(getUsersState, fromAdapter.selectUsersCount);

export const getUsersEntities = createSelector(getUsersState, fromAdapter.selectUsersEntities);

