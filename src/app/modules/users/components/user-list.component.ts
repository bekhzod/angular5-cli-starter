import {Component, OnInit} from '@angular/core';
import {UserDetailsEntity, UserEntity} from '../models/users.model';
import {Observable} from 'rxjs';
import * as fromUsers from '../reducer';
import * as userActions from '../actions/users-actions';
import {select, Store} from '@ngrx/store';
import {UserService} from '../../../common/service/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users$: Observable<UserDetailsEntity[]> = this.store.pipe(select(fromUsers.getUsersList));
  config: {};
  public user: UserEntity = {};

  constructor(private store: Store<fromUsers.State>, private userService: UserService) {
  }

  ngOnInit() {
    this.fetchUsers();
  }

  fetchUsers() {
    this.store.dispatch(new userActions.GetUserList({getConfig: this.getConfig()}));
  }

  getConfig() {
    return this.config;
  }

  saveUser(user: UserDetailsEntity) {
    this.store.dispatch(new userActions.SaveUser({user}));
  }
}
