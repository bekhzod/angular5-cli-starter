import {UserEntity} from '../models/users.model';
import {EntityState} from '@ngrx/entity';

export interface UserStates extends EntityState<UserEntity> {
  //Other entity state properties
}
