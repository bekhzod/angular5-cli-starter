import { createEntityAdapter, EntityAdapter } from '@ngrx/entity';
import { UserDetailsEntity } from '../models/users.model';

export const adapter: EntityAdapter<UserDetailsEntity> = createEntityAdapter<UserDetailsEntity>({
  selectId: ((user: UserDetailsEntity) => user.id),
  sortComparer: false
});

export const {
  selectIds: selectUsersIds,
  selectEntities: selectUsersEntities,
  selectAll: selectAllUsers,
  selectTotal: selectUsersCount
} = adapter.getSelectors();
