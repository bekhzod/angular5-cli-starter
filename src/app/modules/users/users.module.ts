import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { FormsModule } from '@angular/forms';
import { UserListComponent } from './components/user-list.component';
import { StoreModule } from '@ngrx/store';
import { reducers } from './reducer';
import {UsersEffects} from './effects/users-effects';
import {EffectsModule} from '@ngrx/effects';
import {AuthEffects} from '../../core/auth/effects/auth-effects';

const components = [UserListComponent];

@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    FormsModule,
    StoreModule.forFeature('users', reducers),
    EffectsModule.forFeature([UsersEffects, AuthEffects])
  ],
  declarations: components,
  exports: components
})

export class UsersModule {
}
