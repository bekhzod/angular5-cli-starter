import {UserDetailsEntity} from '../models/users.model';

export enum UsersActionsEnum {
  GetUserList = '[Users] GetUserList',
  GetUserListSuccess = '[Users] GetUserListSuccess',
  GetUserListFailed = '[Users] GetUserListFailed',
  SaveUser = '[Users] SaveUser',
  SaveUserSuccess = '[Users] SaveUserSuccess',
  SaveUserFailed = '[Users] SaveUserFailed',
}

export class GetUserList {
  readonly type = UsersActionsEnum.GetUserList;

  constructor(public payload: { getConfig: any }) {
  }
}

export class GetUserListSuccess {
  readonly type = UsersActionsEnum.GetUserListSuccess;

  constructor(public payload: { userList: UserDetailsEntity[] }) {
  }
}

export class GetUserListFailed {
  readonly type = UsersActionsEnum.GetUserListFailed;

  constructor(public payload: { error: String }) {
  }
}

export class SaveUser {
  readonly type = UsersActionsEnum.SaveUser;

  constructor(public payload: { user: UserDetailsEntity }) {
  }
}

export class SaveUserSuccess {
  readonly type = UsersActionsEnum.SaveUserSuccess;

  constructor(public payload: { user: UserDetailsEntity }) {
  }
}

export class SaveUserFailed {
  readonly type = UsersActionsEnum.SaveUserFailed;

  constructor(public payload: { error: String }) {
  }
}


export type UsersActionsList =
  | GetUserList
  | GetUserListSuccess
  | GetUserListFailed
  | SaveUser
  | SaveUserSuccess
  | SaveUserFailed;
