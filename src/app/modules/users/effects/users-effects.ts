import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {UserService} from '../../../common/service/user.service';
import {
  GetUserList, GetUserListFailed, GetUserListSuccess, SaveUser, SaveUserFailed, SaveUserSuccess,
  UsersActionsEnum
} from '../actions/users-actions';
import {catchError, exhaustMap, map} from 'rxjs/internal/operators';
import {of} from 'rxjs';
import {UserDetailsEntity} from '../models/users.model';

@Injectable()
export class UsersEffects {
  constructor(private actions$: Actions, private userService: UserService) {

  }

  @Effect()
  getList$ = this.actions$.pipe(
    ofType<GetUserList>(UsersActionsEnum.GetUserList),
    map(action => action.payload),
    exhaustMap(({getConfig}) =>
      this.userService.selectAll(getConfig).pipe(
        map((userList: UserDetailsEntity[]) => new GetUserListSuccess({userList: userList})),
        catchError(error => of(new GetUserListFailed({error})))
      )
    )
  );

  @Effect()
  saveUse = this.actions$.pipe(
    ofType<SaveUser>(UsersActionsEnum.SaveUser),
    map(action => action.payload),
    exhaustMap(({user}) =>
      this.userService.saveOne(user).pipe(
        map((user: UserDetailsEntity) => new SaveUserSuccess({user})),
        catchError(error => of(new SaveUserFailed({error})))
      ))
  );
}
