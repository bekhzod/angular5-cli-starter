import { BaseEntity } from '../../../common/models/base.model';

export interface UserEntity extends BaseEntity {
  username?: String;
  name?: String;
  password?: String;
}

export interface UserDetailsEntity extends UserEntity {
  address?: String;
  code?: String;
}
