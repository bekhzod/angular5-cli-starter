import {Action, FilmsActionsEnum} from '../actions/films';
import {EntityState} from '@ngrx/entity';
import {Film} from '../../models';
import {adapter} from '../../adapter/films-adapter';
import {FilmsState} from './index';

export interface State extends EntityState<Film> {
  ids: number[];
  films: { [id: number]: Film };
  selected: number;
  error: null
}

export const initialState: State = adapter.getInitialState({
  error: null,
  ids: [1, 2, 3],
  films: {
    1: {
      id: 1, name: 'Interstellar',
      description: 'Interstellar is a 2014 epic science fiction film directed, co-written, and co-produced by Christopher Nolan.',
      img: 'https://goo.gl/8mG12t'
    },
    2: {
      id: 2, name: 'Shutter Island',
      description: 'In 1954, a U.S. Marshal investigates the disappearance of a murderer, who escaped from a hospital for the criminally insane.',
      img: 'https://goo.gl/wfhjUF'
    },
    3: {
      id: 3, name: 'The Grand Budapest Hotel',
      description: 'The adventures of Gustave H, a legendary concierge at',
      img: 'https://goo.gl/mDBt45'
    }
    ,
  },
  selected: null,
});

export function reducer(state = initialState,
                        action: Action) {
  console.log('reducer', state, action.type);
  switch (action.type) {

    case FilmsActionsEnum.SelectListSuccess: {
      return {...state, films: action.payload};
    }
    case FilmsActionsEnum.SelectOne: {
      return {...state, selected: action.payload};
    }
    case FilmsActionsEnum.SelectOneSuccess: {
      return {
        ...state,
        film: action.payload
      };
    }
    default: {
      return state;
    }

  }
}

export const getFilm = (state: FilmsState) => state.films;
export const getIds = (state: State) => state.ids;
export const getFilms = (state: State) => state.films;
export const getSelected = (state: State) => state.selected;
export const getSelectedFilms = (selectedId, films) => ({...films[selectedId]});
export const getAllFilms = (ids, films) => ids.map(id => films[id]);
