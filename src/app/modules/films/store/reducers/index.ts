import * as fromFilms from './films';
import {
  ActionReducerMap, createSelector, createFeatureSelector,
  ActionReducer, MetaReducer
} from '@ngrx/store';
import {Film} from '../../models';

export interface FilmsState {
  films: fromFilms.State;
}

export const reducers: ActionReducerMap<FilmsState> = {
  films: fromFilms.reducer
};

export function logger(reducer: ActionReducer<FilmsState>): ActionReducer<FilmsState> {
  return function (state: FilmsState, action: any): FilmsState {
    return reducer(state, action);
  };
}

export const getFilmState = createFeatureSelector<FilmsState>('films');

export const getFilm = createSelector(getFilmState, fromFilms.getFilm);

export const getIds = createSelector(getFilm, fromFilms.getIds);

export const getFilms = createSelector(getFilm, fromFilms.getFilms);

export const getSelected = createSelector(getFilm, fromFilms.getSelected);

export const getSelectedFilm = createSelector(getSelected, getFilms, fromFilms.getSelectedFilms);

export const getAllFilms = createSelector(getIds, getFilms, fromFilms.getAllFilms);
