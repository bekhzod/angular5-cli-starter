import { Action } from '@ngrx/store';
import { Film } from "../../models/index";

export enum FilmsActionsEnum {
  Select = '[Films] SelectList',
  SelectListSuccess = '[Films] SelectListSuccess',
  SelectOne = '[Films] SelectOne',
  SelectOneSuccess = '[Films] SelectOneSuccess',
}

export class SelectList implements Action {
  readonly type = FilmsActionsEnum.Select;

  constructor(public payload: any) {
  }
}

export class SelectListSuccess implements Action {
  readonly type = FilmsActionsEnum.SelectListSuccess;

  constructor(public payload: Film[]) {
  }
}

export class SelectOne implements Action {
  readonly type = FilmsActionsEnum.SelectOne;
  constructor(public payload: number) {
  }
}

export class SelectOneSuccess implements Action {
  readonly type = FilmsActionsEnum.SelectOneSuccess;

  constructor(public payload: { film: Film }) {
  }
}

export type Action = SelectList
  | SelectListSuccess
  | SelectOneSuccess
  | SelectOne;
