import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FilmRootComponent} from "./components/film-root/film-root.component";

const routes: Routes = [
  {
    path: '',
    component: FilmRootComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FilmsRoutingModule {
}
