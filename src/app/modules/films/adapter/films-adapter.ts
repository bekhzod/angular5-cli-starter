import {createEntityAdapter, EntityAdapter} from '@ngrx/entity';
import {Film} from '../models';

export const adapter: EntityAdapter<Film> = createEntityAdapter<Film>({
  sortComparer: false
});
