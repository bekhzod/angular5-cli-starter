import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FilmsRoutingModule} from "./films-routing.module";
import {FilmsListComponent} from './components/films-list/films-list.component';
import {FilmSelectedComponent} from './components/film-selected/film-selected.component';
import {FilmRootComponent} from './components/film-root/film-root.component';
import {FormsModule} from "@angular/forms";
import {FilmsComponent} from "./components/films-item/films.component";
import {StoreModule} from "@ngrx/store";
import {reducers} from "./store/reducers";

const components = [FilmRootComponent, FilmsComponent, FilmsListComponent, FilmSelectedComponent];

@NgModule({
  imports: [
    CommonModule,
    FilmsRoutingModule,
    FormsModule,
    StoreModule.forFeature('films', reducers),
  ],
  exports: components,
  declarations: components
})
export class FilmsModule {
}
