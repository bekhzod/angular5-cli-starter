import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilmRootComponent } from './film-root.component';

describe('FilmRootComponent', () => {
  let component: FilmRootComponent;
  let fixture: ComponentFixture<FilmRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilmRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilmRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
