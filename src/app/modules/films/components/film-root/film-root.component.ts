import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs/index";
import {Film} from "../../models";
import {Store} from "@ngrx/store";

import * as fromRoot from '../../store/reducers';
import * as filmAction from '../../store/actions/films';

@Component({
  selector: 'app-film-root',
  templateUrl: './film-root.component.html',
  styleUrls: ['./film-root.component.css']
})
export class FilmRootComponent implements OnInit {


  films$: Observable<Film[]>;
  selected$: Observable<any>;

  constructor(private store: Store<fromRoot.FilmsState>) {
    this.films$ = store.select(fromRoot.getAllFilms);
    this.selected$ = store.select(fromRoot.getSelectedFilm);
  }

  ngOnInit() {
  }

  onSelect(id: number) {
    console.log('onSelect');
    this.store.dispatch(new filmAction.SelectOne(id));
  }

}
