import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Film} from "../../models/index";

@Component({
  selector: 'app-films-list',
  templateUrl: './films-list.component.html',
  styleUrls: ['./films-list.component.css']
})
export class FilmsListComponent implements OnInit {
  @Input() films: Film[];
  @Input() label: string;
  @Output() select = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

}
