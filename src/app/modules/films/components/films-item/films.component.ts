import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Film} from "../../models/index";

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit {

  @Input() film: Film;
  @Output() select = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
