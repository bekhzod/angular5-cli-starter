import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

import {
  HttpErrorResponse, HttpEvent, HttpHandler,
  HttpRequest, HttpResponse, HttpInterceptor
} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {AuthGuard} from '../service/guards/auth-service';


@Injectable({
  providedIn: 'root'
})
export class TokenInterceptor implements HttpInterceptor {
  constructor(private router: Router, private authService: AuthGuard) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    req = req.clone({
      setHeaders: {
        'AUTH-TOKEN-UUID': `${localStorage.getItem('X-AUTH-TOKEN')}`,
        responseType: 'arraybuffer' as 'json'
      }
    });
    return next.handle(req)
      .pipe(tap(this.validateHandler, this.errorHandler));
  }

  validateHandler(event) {
    if (event instanceof HttpResponse) {
      const response = event.clone();
      if (response.body && response.body.data) {
        response.body.data = JSON.parse(atob(response.body.data));
      }
      return response;
    }
  }

  async errorHandler(error) {
    if (error instanceof HttpErrorResponse) {
      if (error.status === 401 || error.status === 403) {
        await this.authService.removeToken();
        await this.router.navigateByUrl('/login');
      }
    }
  }
}
