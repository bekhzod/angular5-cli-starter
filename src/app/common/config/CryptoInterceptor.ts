import {Observable} from 'rxjs';
import {
  HttpEvent, HttpHandler, HttpInterceptor, HttpRequest,
} from '@angular/common/http';
import {Injectable} from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class CryptoInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.method === 'POST' && req.body) {
      req.body.data = btoa(JSON.stringify(req.body));
      Object.keys(req.body).map(prop => {
        if (prop !== 'data') { delete req.body[prop]; }
      });
      console.log(req);
    }
    return next.handle(req);
  }
}
