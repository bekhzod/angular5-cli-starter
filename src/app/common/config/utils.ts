export const objectToArray = (object) => Object.keys(object).map((index) => object[index]);
