import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivateChild, RouterStateSnapshot} from "@angular/router";

import {AuthGuard} from "./auth-service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuardChild implements CanActivateChild {

  constructor(private authService: AuthGuard) {
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> | boolean {
    return this.authService.validate();
  }

}
