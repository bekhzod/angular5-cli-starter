import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Credentials} from '../../config/types';
import {ok} from 'assert';
import {UserService} from '../user.service';
import {cryptoWrapper} from '../base.service';
import {ResponseDataEntity, TokenEntity} from '../../models/base.model';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  public requestedUrl = '/';
  public user = false;
  public tokenValidated = false;


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Promise<boolean> | Observable<boolean> {
    this.requestedUrl = state.url;
    return this.validate();
  }


  constructor(private http: HttpClient, private router: Router, private userService: UserService) {
  }

  public async validate() {
    const token = this.getToken();
    if (!token) {
      this.redirectToLogin();
      return false;
    }
    await this.checkToken();
    if (this.tokenValidated && this.userService.user) return true;
  }

  async checkToken() {
    const data = await cryptoWrapper(this.http.get<any>('/api/token/check')).toPromise();
    if (data) {
      this.userService.user = data;
      this.tokenValidated = true;
    } else {
      this.userService.user = false;
      this.tokenValidated = false;
    }
  }

  private setAuthTokenId(token): boolean {
    if (token.hasOwnProperty('data') && token.data) { token = token.data; }
    localStorage.setItem('X-AUTH-TOKEN', token);
    return true;
  }

  private getToken() {
    return localStorage.getItem('X-AUTH-TOKEN');
  }

  public async removeToken() {
    console.error('token will be removed');
    this.user = false;
    this.tokenValidated = false;
    localStorage.removeItem('X-AUTH-TOKEN');
  }

  public redirectToLogin() {
    this.redirect('/login');
  }

  public async redirect(url) {
    await this.router.navigateByUrl(url);
  }

  public async authenticate(credentials: Credentials) {
    if (!this.getToken()) {
      await this.generate(credentials.login, credentials.password).subscribe((data: TokenEntity) => {
        if (data) {
          this.setAuthTokenId(data);
          this.redirect(this.requestedUrl);
        } else {
          this.redirectToLogin();
        }
      });
    }
  }

  public generate(login, password) {
    ok(login, password);
    const headers = AuthGuard.getAuthorizationHeader(login, password);
    return cryptoWrapper(this.http.get<ResponseDataEntity<TokenEntity>>('/api/token/generate', {headers}));
  }

  public static getAuthorizationHeader(login, password) {
    const basic: String = btoa(login + ':' + password);
    return {
      Credentials: 'Basic ' + basic
    };
  }
}
