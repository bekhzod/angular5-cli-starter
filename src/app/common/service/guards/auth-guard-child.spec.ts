import {TestBed, inject} from '@angular/core/testing';
import {AuthGuardChild} from "./auth-guard-child";


describe('AuthGuardChild', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthGuardChild]
    });
  });

  it('should be created', inject([AuthGuardChild], (service: AuthGuardChild) => {
    expect(service).toBeTruthy();
  }));
});
