import {HttpClient} from '@angular/common/http';
import {BaseEntity, Options, ResponseDataEntity} from '../models/base.model';
import {ok} from 'assert';
import {map} from 'rxjs/internal/operators';
import {Observable} from 'rxjs';

export class BaseService<T extends BaseEntity> {

  rootUrl: string;

  constructor(protected http: HttpClient, rootUrl: string) {
    this.rootUrl = rootUrl;
  }

  selectAll(options: Options) {
    return cryptoWrapper(this.http.get<ResponseDataEntity<T>>(this.rootUrl, options));
  }

  selectOne(id) {
    ok(id);
    return cryptoWrapper(this.http.get<ResponseDataEntity<T>>(`${this.rootUrl}/${id}`));
  }

  updateOne(id, body) {
    ok(id);
    return cryptoWrapper(this.http.put<ResponseDataEntity<T>>(`${this.rootUrl}/${id}`, body));
  }

  deleteOne(id) {
    ok(id);
    return cryptoWrapper(this.http.delete<ResponseDataEntity<T>>(`${this.rootUrl}/${id}`));
  }

  saveOne(body) {
    ok(body);
    if (body.id) {
      return this.updateOne(body.id, body);
    } else {
      if (Array.isArray(body)) {
        return Promise.all([body.map(element => this.saveOne(element))]);
      } else {
        return cryptoWrapper(this.http.post<ResponseDataEntity<T>>(`${this.rootUrl}`, body));
      }
    }
  }
}


export const cryptoWrapper = <T extends BaseEntity>(object: Observable<ResponseDataEntity<T>>) => object.pipe(map(({data}) => data));
