import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserDetailsEntity } from '../../modules/users/models/users.model';
import { Observable } from 'rxjs/index';
import { catchError } from 'rxjs/internal/operators';
import { BaseService } from './base.service';
import { constants } from '../constants';

@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseService<UserDetailsEntity> {

  public user:any = false;

  constructor(http: HttpClient) {
    super(http, constants.rootMapping.users);
  }


  public async getCurrentUser() {
    return this.user;
  }

  public userList(params) {
    console.log('it is working');
    return this.http.get<UserDetailsEntity[]>('/api/user', { params }).pipe(catchError((error: any) => Observable.throw(error.json())));
  }

}
