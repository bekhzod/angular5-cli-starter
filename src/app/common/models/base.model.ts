import { HttpParams } from '@angular/common/http/src/params';
import { HttpHeaders } from '@angular/common/http/src/headers';

export interface BaseEntity {
  id?: number;
  created_at?: Date;
  created_by?: number;
}

export interface ResponseDataEntity<T extends BaseEntity> {
  data: T | T[] | null;
}

export interface Options {
  headers?: HttpHeaders | {
    [header: string]: string | string[];
  };
  observe?: 'body';
  params?: HttpParams | {
    [param: string]: string | string[];
  };
  reportProgress?: boolean;
  responseType?: 'json';
  withCredentials?: boolean;
}
export interface TokenEntity extends BaseEntity{
  uuid?:String,
  userId?:number,
  username?:String
}
