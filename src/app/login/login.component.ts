import {Component, HostBinding, OnInit} from '@angular/core';
import {AuthGuard} from '../common/service/guards/auth-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public username: String;
  public password;

  @HostBinding('class') classes = 'd-flex flex-column h-100';

  constructor(private authService: AuthGuard) {
  }

  async send() {
    await this.authService.authenticate({login: this.username, password: this.password});
  }

  ngOnInit() {
  }
}
